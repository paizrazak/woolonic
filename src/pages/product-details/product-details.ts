import { CartPage } from './../cart/cart';
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import * as WC from "woocommerce-api";
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
})
export class ProductDetailsPage {

  product: any;
  WooCommerce: any;
  reviews: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage:Storage, public toastCtrl:ToastController, public modalCtrl:ModalController) {
    this.product = this.navParams.get("product");
    console.log(this.product);

    this.WooCommerce = WC({
      url: "http://localhost/wordpress/",
      consumerKey: "ck_c842bb5c17774ae1b13032ec20d7438ca0cdb816",
      consumerSecret: "cs_ba6f94fc8bf4497e9d656fba6516d6fa4775bd0c"
    });

    this.WooCommerce.getAsync('products/' + this.product.id + '/reviews').then((data) => {

      this.reviews = JSON.parse(data.body).product_reviews;

    }, (err) => {
      console.error(err);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductDetailsPage');
  }

  addToCart(product){

    this.storage.get("cart").then((data) => {

      if(data ==  null || data == 0){
        data = [];
        data.push({
          "product": product,
          "qty": 1,
          "amount": parseFloat(product.price)
        });

      } else {
        let added = 0;

        for (let i = 0; i < data.length; i++) {

          if (product.id == data[i].product.id) {
            console.log("Product is already in the cart");
            let qty = data[i].qty;
            data[i].qty = qty+1;
            data[i].amount = parseFloat(data[i].amount) + parseFloat(data[i].product.price);
            added = 1;
          }

        }

        if (added == 0) {
          data.push({
            "product": product,
            "qty": 1,
            "amount": parseFloat(product.price)
          });

        }

      }

      this.storage.set("cart", data).then(()=>{
        console.log("cart updated");
        console.log(data);

        this.toastCtrl.create({
          message: "Cart Updated",
          duration: 3000
        }).present();

      });

    });

  }

  openCart(){
    this.modalCtrl.create(CartPage).present();
  }

}
