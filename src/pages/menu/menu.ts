import { CartPage } from './../cart/cart';
import { Storage } from '@ionic/storage';
import { LoginPage } from './../login/login';
import { SignupPage } from './../signup/signup';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import * as WC from "woocommerce-api";
import { HomePage } from './../home/home';
import { ProductsByCategoryPage } from '../products-by-category/products-by-category';

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class Menu {

  homePage: any;
  WooCommerce: any;
  categories: any[];
  @ViewChild('content') childNavCtrl: NavController;
  loggedIn: boolean;
  user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage:Storage, public modalCtrl:ModalController) {
    
    this.homePage = HomePage
    
    this.categories = [];

    this.user = {};

    this.WooCommerce = WC({
      url: "http://localhost/wordpress/",
      consumerKey: "ck_c842bb5c17774ae1b13032ec20d7438ca0cdb816",
      consumerSecret: "cs_ba6f94fc8bf4497e9d656fba6516d6fa4775bd0c"
    });

    this.WooCommerce.getAsync("products/categories").then((data) => {
      console.log(JSON.parse(data.body).product_categories);

      let temp: any[] = JSON.parse(data.body).product_categories;

      for(let i = 0; i < temp.length; i++){
        if (temp[i].parent == 0) {

          if (temp[i].slug == "clothing") {
            temp[i].icon = "shirt";
          }

          if (temp[i].slug == "decor") {
            temp[i].icon = "cube";
          }

          if (temp[i].slug == "music") {
            temp[i].icon = "musical-notes";
          }

          this.categories.push(temp[i]);
        }
      }

    }, (err) => {
      console.log(err);
    })
  }

  ionViewDidEnter() {
    this.storage.ready().then(()=>{
      this.storage.get("userLoginInfo").then((userLoginInfo)=>{
        if (userLoginInfo != null) {
          console.log("user logged in..");
          this.user = userLoginInfo.user;
          console.log(this.user);
          this.loggedIn = true;
        } else {
          console.log("No user found");
          this.user = {};
          this.loggedIn = false;
        }
      })
    })
  }

  openCategoryPage(category){
    this.childNavCtrl.setRoot(ProductsByCategoryPage, { "category": category });
  }

  openPage(pageName: string){
    if(pageName == 'signup'){
      this.navCtrl.push(SignupPage);
    }
    if(pageName == 'login'){
      this.navCtrl.push(LoginPage);
    }
    if(pageName == 'logout'){
      this.storage.remove("userLoginInfo").then(() => {
        this.user = {};
        this.loggedIn = false;
      })
    }
    if(pageName == 'cart'){
      let modal = this.modalCtrl.create(CartPage);
      modal.present();
    }
  }

}
